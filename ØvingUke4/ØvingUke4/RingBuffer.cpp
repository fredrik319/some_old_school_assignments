#include "stdafx.h"
#include "RingBuffer.h"

RingBuffer::RingBuffer(){}

RingBuffer::RingBuffer(int capacity) {
	buffer = new char[capacity];
	bufferSize = capacity;
}

RingBuffer::~RingBuffer()
{
	if (buffer != nullptr) {
		delete[] buffer;
	} 
}

void RingBuffer::add(char val) {
	std::unique_lock<std::mutex> lock(mutex);
	while ((in + 1) % bufferSize == out) {
		full_cv.wait(lock);
	}

	buffer[in] = val;
	in = (in + 1) % bufferSize;
	empty_cv.notify_one();
}

char RingBuffer::get() {
	std::unique_lock<std::mutex> lock(mutex);
	while (in == out) {
		empty_cv.wait(lock);
	}

	int consumed = buffer[out];
	out = (out + 1) % bufferSize;
	full_cv.notify_one();
	  
	return consumed; 
}

int RingBuffer::getnum() {      // fuksjon for � skrive ut hvilken plass i ringbufferet som det legges til i
	int num;
    num = in;
	return num;	
}


 