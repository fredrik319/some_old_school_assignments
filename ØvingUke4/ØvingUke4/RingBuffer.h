#pragma once

#include <mutex>
#include <condition_variable>
class RingBuffer
{
public:
	RingBuffer();
	RingBuffer(int capacity);

	~RingBuffer();

	void add(char val);
	char get();
	int getnum();

private:
	std::mutex mutex;
	std::condition_variable full_cv;
	std::condition_variable empty_cv;

	char* buffer;
	int bufferSize;
	int in = 0;
	int out = 0;
};

