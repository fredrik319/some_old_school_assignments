// �vingUke4.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <thread>
#include <iostream>
#include "string"
#include "RingBuffer.h"


void generator(RingBuffer * buffer) {                               //Funksjon for � legge til generert tekst i ringbufferet
	while (true) {
		std::string gen = "Generert text";
		for (int i = 0; i < gen.size(); i++) {
			buffer->add(gen[i]);                                   //Legger til stringen oppdelt i char, p� ringbufferet
			std::this_thread::sleep_for(std::chrono::seconds(1));  // venter p� at en og en char skal legge seg til f�r det skjer noe mer
		}
		buffer->add('\n');	
	}
}

void bufferReader(RingBuffer * buffer) {
	while (true) {
		std::cout << buffer->get() << " ";
		std::cout << buffer->getnum() << "\n";       // Skriver ut hvilken plass i ringbufferet som skrives ut	
	}   
}

void keyboardReader(RingBuffer * buffer) {
	while (true) {
		std::string input;
		std::getline(std::cin,input);
		for (int i = 0; i < input.size(); i++) {
			buffer->add(input[i]);
		}
		buffer->add('\n'); 
	}
}

int main()
{
	RingBuffer buffer(10);

	std::thread keyboard(keyboardReader, &buffer);              // Tr�dene for � lese inn via tastatur, lese inn det genererte og for � skrive ut
	std::thread generator(generator, &buffer);
	std::thread reader(bufferReader, &buffer);

	keyboard.join();
	reader.join();
	generator.join();
    return 0;
}

