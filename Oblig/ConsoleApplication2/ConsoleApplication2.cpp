// ConsoleApplication2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

int tall1;                     //tallene som skal brukes
int tall2;
char oper;                    // operasjonen som skal brukes

int kalk(int a,char op, int b) {     //Funksjonen som gj�r standardtingene
	switch (op) {
		case'+':
			return a + b;               // om operasjonen er + legges tallene sammen
		
		case'-':                        // trekkes fra om operasjonen er -
			return a - b;
		
		case'*':                        // samme med gange
			return a*b;
		
		case'/':                        // og dele
			return a / b;
	
		default:
			std::cout << "skriv inn en regneoperasjon";
	}
}

int fakultet(int tall) {                 //Funksjonen som regner ut fakultet 
	int svar = 1;
	for (int i = 1; i <= tall; i++) {         // for hvert tall opp til tallet som blir skrevet inn
		svar = i * svar;                      // ganges det tallet man er p� med det forrige, 
	}
	return svar;                              //resultatet returneres 
}

int fakultetREK(int tall) {              //Funksjonen som regner ut fakultet med rekursjon
	int svar = 1;
	if (tall <= 1) {                     //hvis tallet som blir skrevet inn er mindre enn 1 skjer ingenting
		return 1;
	}
	else{
		svar = tall*fakultetREK(tall - 1);     //ellers ganges tallet med det tallet under det funksjonen var p� sist helt til det tallet blir 1
		return svar;                           // da returneres svaret
	}
}

void faktoriser(int tall) {                     // funksjon for � faktorisere
	for (int i = 2; i <= tall; i++) {           // for alle tall fra 2 til det innskrevne
		while (tall%i == 0) {                   // mens tallet, delt p� tallet som sjekkes i for-l�kka , blir et heltall
			tall = tall / i;                    // s� skal tallet deles p� dette
			std::cout << i << " ";              //og si ifra hvilket tall det ble delt p� 
		}
	}
}

int main()
{
	int fakulsvar;                         //til fakultet
	int fakREK;
	int fakREKsvar;  

	int faktor;                           //til faktorisering
	int faktorsvar;

	int svar;                            // til standardregning
	
	std:: cout << "tall1: ";         //Skriver inn ett tall
	std:: cin >> tall1;

	std::cout << "Operator: +, -, *eller / . !for fakultet og # for faktorisering" << std::endl;       // skriver inn hva man vil gj�re med tallet
	std::cin >> oper;

	if (oper == '!') {                // hvis man vil finne fakultet
		fakulsvar = fakultet(tall1);
		std::cout << "svaret er: " << fakulsvar << "\n";      //settes tallet inn i fakultetfunksjonen og man f�r svaret

		std::cout << "Fakultet med rekursjon: ";              // dette er for � vise at rekursjonfunksjonen ogs� fungerer
		std::cin >> fakREK;

		fakREKsvar = fakultetREK(fakREK);
		std::cout << "svaret er: " << fakREKsvar;
		return 0;
	}
	else if (oper == '#') {                             //hvis man vil faktorisere
		std::cout << "Faktoriser tall: ";               // settes tallet inn i faktoriseringsfunksjonen og man f�r svaret
		faktoriser(tall1);
		return 0;
	}
	else {                                      // Ellers s� kan man gj�re standard regnefunksjonene
		std::cout << "tall2: ";                 // fyller inn tall2
		std::cin >> tall2;

		svar = kalk(tall1, oper, tall2);        // kalkulatoren utf�rer regneoperasjonen og gir svaret
		std::cout << "svaret er: " << svar << "\n";
	}
  return 0;
}

